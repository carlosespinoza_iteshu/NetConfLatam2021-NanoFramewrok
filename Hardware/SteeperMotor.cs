﻿using System.Device.Gpio;
using System.Threading;

namespace Hardware
{
    public class SteeperMotor
    {
        private GpioPin In1, In2, In3, In4;
        private int pos;
        private PinValue[][] secuencia;
        public int Frecuencia { get; set; }
        public void Girar()
        {
            Mandar(secuencia[pos++]);
            if (pos == 4)
            {
                pos = 0;
            }
        }
        public void Girar(int pasos)
        {
            for (int i = pos; i < (pos + pasos); i++)
            {
                Mandar(secuencia[pos++]);
                if (pos == 4)
                {
                    pos = 0;
                }
                Thread.Sleep(Frecuencia);
            }
        }

        public void GirarGrados(int grados)
        {
            Girar(grados * 2045 / 360);
        }


        private void Mandar(PinValue[] pulsos)
        {
            In1.Write(pulsos[0]);
            In2.Write(pulsos[1]);
            In3.Write(pulsos[2]);
            In4.Write(pulsos[3]);
        }
        public SteeperMotor(GpioPin in1, GpioPin in2, GpioPin in3, GpioPin in4, int frecuecia)
        {
            In1 = in1;
            In2 = in2;
            In3 = in3;
            In4 = in4;
            Frecuencia = frecuecia;
            secuencia = new PinValue[][] {
               new PinValue[] { PinValue.High, PinValue.Low, PinValue.Low, PinValue.Low },
               new PinValue[] { PinValue.Low, PinValue.High, PinValue.Low, PinValue.Low },
               new PinValue[] { PinValue.Low, PinValue.Low, PinValue.High, PinValue.Low },
               new PinValue[] { PinValue.Low, PinValue.Low, PinValue.Low, PinValue.High }
            };
            Mandar(secuencia[0]);
            pos = 0;
        }
    }
}
