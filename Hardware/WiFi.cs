﻿using nanoFramework.Networking;
using System;
using System.Device.Gpio;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Threading;

namespace Hardware
{
    public class WiFi
    {
        public bool Conectado { get; private set; }
        public string IP { get; private set; }
        public string MAC { get; private set; }

        private readonly GpioPin ledStatus;
        private readonly Timer conectando;
        public WiFi(string ssid, string password, GpioPin ledStatus)
        {
            Debug.WriteLine("Esperando conexión...");
            Conectado = false;
            this.ledStatus = ledStatus;
            conectando = new Timer(ConectandoWiFiTimer, null, 500, 500);
            CancellationTokenSource cs = new(60000);
            Conectado = NetworkHelper.ConnectWifiDhcp(ssid, password, setDateTime: true, token: cs.Token);

            if (!Conectado)
            {
                Debug.WriteLine($"No se puede obtener una IP y fecha hora Adecuadas, error: {NetworkHelper.ConnectionError.Error}.");
                if (NetworkHelper.ConnectionError.Exception != null)
                {
                    Debug.WriteLine($"Error: {NetworkHelper.ConnectionError.Exception}");
                }
                return;
            }
            else
            {
                ObtenerIPyMAC();
                Debug.WriteLine($"[{DateTime.UtcNow}] Conectado a: {ssid} con IP:{IP} :-)");
            }


        }
        private void ConectandoWiFiTimer(object state)
        {
            if (!Conectado)
            {
                ledStatus.Toggle();
            }
            else
            {
                ledStatus.Write(PinValue.High);
                conectando.Dispose();
            }
        }
        private void ObtenerIPyMAC()
        {
            while (true)
            {
                NetworkInterface ni = GetInterface();
                
                if (ni.IPv4Address != null && ni.IPv4Address.Length > 0)
                {
                    if (ni.IPv4Address[0] != '0')
                    {
                        IP = ni.IPv4Address;
                        MAC = ni.PhysicalAddress.ToString();
                        break;
                    }
                }
                Thread.Sleep(200);
            }
        }

        private NetworkInterface GetInterface()
        {
            NetworkInterface[] Interfaces = NetworkInterface.GetAllNetworkInterfaces();

            // Find WirelessAP interface
            foreach (NetworkInterface ni in Interfaces)
            {
                if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                {
                    return ni;
                }
            }
            return null;
        }
    }
}
