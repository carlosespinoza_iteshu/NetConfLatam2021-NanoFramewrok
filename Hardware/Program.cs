using nanoFramework.M2Mqtt;
using nanoFramework.Networking;
using System;
using System.Device.Gpio;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using Windows.Devices.Adc;


namespace Hardware
{
    public class Program
    {
        private static WiFi wifi;
        private static SteeperMotor motor;

        private static GpioController esp32D;
        private static GpioPin lRojo, lVerde, luces, humedadTierraDigital, buzzer, button, movimiento;
        private static GpioPin m1, m2, m3, m4;
        private static AdcController esp32A;
        private static AdcChannel potenciometro, humedadTierra, phoresistencia;
        private static Timer leerVariables, iotCloud, buttonTimer;

        private static bool lucesEncendidas = false;
        private static bool buzzerEncendido = false;
        
        /*
        private static string Ssid = "Zero's WiFi";
        private static string WifiPassword = "qwertyuiop99";
        */
        
        private static string Ssid = "INFINITUM308C_2.4";
        private static string WifiPassword = "5nNUF2Rdhb";

        private static int contador = 0;
        private const string MqttBroker = "broker.hivemq.com";

        private static MqttClient mqtt;
        public static void Main()
        {
            Debug.WriteLine("Hello from nanoFramework!");
            esp32D = new GpioController();
            esp32A = AdcController.GetDefault();
            
            potenciometro = esp32A.OpenChannel(6);
            humedadTierra = esp32A.OpenChannel(7);
            phoresistencia = esp32A.OpenChannel(4);

            humedadTierraDigital = esp32D.OpenPin(33, PinMode.Input);
            lRojo = esp32D.OpenPin(21, PinMode.Output);
            lVerde = esp32D.OpenPin(22, PinMode.Output);
            luces = esp32D.OpenPin(19, PinMode.Output);
            button = esp32D.OpenPin(25, PinMode.Input);
            movimiento = esp32D.OpenPin(27, PinMode.Input);

            m1 = esp32D.OpenPin(18, PinMode.Output);
            m2 = esp32D.OpenPin(5, PinMode.Output);
            m3 = esp32D.OpenPin(17, PinMode.Output);
            m4 = esp32D.OpenPin(16, PinMode.Output);

            buzzer = esp32D.OpenPin(15, PinMode.Output);


            lRojo.Write(PinValue.Low);
            try
            {
                wifi = new WiFi(Ssid, WifiPassword, lVerde);
                if (wifi.Conectado)
                {
                    mqtt = new MqttClient(MqttBroker, 1883, false, null, null, MqttSslProtocols.None);
                    mqtt.MqttMsgPublishReceived += Mqtt_MqttMsgPublishReceived;
                    mqtt.Subscribe(new string[] { "NFDemo2/Hw_In" }, new[] { nanoFramework.M2Mqtt.Messages.MqttQoSLevel.AtMostOnce });
                    mqtt.Connect("NFDemo2_ESP32");
                    motor = new SteeperMotor(m1, m2, m3, m4, 5);
                    luces.Write(PinValue.Low);
                    mqtt.Publish("NFDemo2/Hw_Out", Encoding.UTF8.GetBytes("ESP32 Conectada"));
                    leerVariables = new Timer(LeerVariables, null, 1000, 5000);
                    buttonTimer = new Timer(RevisarBoton, null, 2000, 100);
                    movimiento.ValueChanged += Movimiento_ValueChanged;
                    //iotCloud = new Timer(CargarDatos, null, 5000, 5*(60*1000));

                }
            }
            catch (Exception ex)
            {
                lRojo.Write(PinValue.High);
                Debug.WriteLine(ex.Message);
            }
            Thread.Sleep(Timeout.Infinite);
                       
        }

        private static void Movimiento_ValueChanged(object sender, PinValueChangedEventArgs e)
        {
            if(e.ChangeType== PinEventTypes.Rising)
            {
                mqtt.Publish("NFDemo2/Hw_Out", Encoding.UTF8.GetBytes("Movimiento"));
            }
        }

        private static void RevisarBoton(object state)
        {
            if(button.Read()== PinValue.High)
            {
                contador++;
                if(contador==int.MaxValue)
                {
                    contador = 0;
                }
            }
        }

        private static void CargarDatos(object state)
        {
            Debug.WriteLine("==========> Cargando datos a IoTCloud");
            CargarIoTCloud("6153d09ce885866490de7af4", float.Parse((potenciometro.ReadRatio() * 100).ToString()));
            CargarIoTCloud("6153f8fbd2bbe450405c1205", float.Parse((humedadTierra.ReadRatio() * 100).ToString()));
            Debug.WriteLine("==========> Carga terminada");
        }

        private static void CargarIoTCloud(string idSensor, float valor)
        {
            Debug.WriteLine("Cargando en IoTCloud");
            string url = "http://iotcloud2019.azurewebsites.net/api/Lectura?user=cespinoza@iteshu.edu.mx&pass=123456&idSensor=" + idSensor + "&valor=" + valor;
            Debug.WriteLine(url);
            var httpWebRequest = WebRequest.Create(url);
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.ContentLength = 0;
            try
            {

                /*
                 * HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                byte[] buffer = new byte[1024];
                int bytesRead = 0;
                using (Stream stream = httpWebResponse.GetResponseStream())
                {
                    bytesRead = stream.Read(buffer, 0, buffer.Length);
                    string Stream = Encoding.UTF8.GetString(buffer, 0, bytesRead);
                    Debug.WriteLine(Stream.Length.ToString());
                }
                */
                httpWebRequest.GetResponse();
                //httpWebResponse.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"No puedo cargar datos al sensor: {idSensor}; Error: {ex.Message}");
            }
            finally
            {
                httpWebRequest.Dispose();
            }
            
           

        }

        private static void Mqtt_MqttMsgPublishReceived(object sender, nanoFramework.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            string mensaje = UTF8Encoding.UTF8.GetString(e.Message, 0, e.Message.Length);
            Debug.WriteLine($"Recibido: {mensaje}");
            switch (mensaje[0])
            {
                case 'L':
                    if (mensaje[1] == '1')
                    {
                        luces.Write(PinValue.High);
                        lucesEncendidas = true;
                    }
                    else
                    {
                        luces.Write(PinValue.Low);
                        lucesEncendidas = false;
                    }
                    break;
                case 'M':
                    motor.GirarGrados(int.Parse(mensaje.Substring(1, mensaje.Length - 1)));
                    break;
                case 'B':
                    if (mensaje[1] == '1')
                    {
                        buzzer.Write(PinValue.High);
                        buzzerEncendido = true;
                    }
                    else
                    {
                        buzzer.Write(PinValue.Low);
                        buzzerEncendido = false;
                    }
                    break;
                default:
                    break;
            }
        }


        private static void LeerVariables(object state)
        {
            Debug.WriteLine(DateTime.UtcNow.AddHours(-6).ToString());
            Debug.WriteLine($"Potenciómetro: {potenciometro.ReadRatio()*100}% {potenciometro.ReadValue()}");
            Debug.WriteLine($"Humedad en tierra: {humedadTierra.ReadRatio()*100}% {humedadTierra.ReadValue()} {humedadTierraDigital.Read()}");
            Debug.WriteLine($"Luminosidad: {phoresistencia.ReadRatio()*100}% {phoresistencia.ReadValue()}");
            Debug.WriteLine($"Estatus Buzzer: {buzzerEncendido}");
            Debug.WriteLine($"Estatus Luces: {lucesEncendidas}");
            Debug.WriteLine($"Contador: {contador}");

            string mensaje = $"Variables|{potenciometro.ReadRatio() * 100}|{humedadTierra.ReadRatio() * 100}|{phoresistencia.ReadRatio() * 100}|{buzzerEncendido}|{lucesEncendidas}|{contador}";

            mqtt.Publish("NFDemo2/Hw_Out", Encoding.UTF8.GetBytes(mensaje));

        }
    }
}
