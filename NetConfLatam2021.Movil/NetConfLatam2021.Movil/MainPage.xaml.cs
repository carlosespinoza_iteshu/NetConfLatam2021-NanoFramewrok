﻿using Microcharts;
using NetConfLatam2021.Movil.Modelos;
using OpenNETCF.MQTT;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace NetConfLatam2021.Movil
{
    public partial class MainPage : ContentPage
    {
        private MQTTClient clientMqtt;
        private bool nuevoMensaje = false;
        private bool nuevoMovimiento = false;
        private List<Mensaje> mensajes;
        private List<Movimiento> movimientos;
        private List<Microcharts.ChartEntry> lecturas;
        private float valHumedad = 0;
        private float valPotenciometro = 0;
        private float valLuminosidad = 0;
        private bool lucesEncendidas = false;
        private bool BuzerActivo = false;
        private int valContador = 0;

        public MainPage()
        {
            InitializeComponent();
            mensajes = new List<Mensaje>();
            movimientos = new List<Movimiento>();
            lecturas = new List<Microcharts.ChartEntry>();
        }

        private void ClientMqtt_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            string mensaje = Encoding.UTF8.GetString(payload);
            mensajes.Add(new Mensaje(topic, mensaje));
            nuevoMensaje = true;
            if (topic == "NFDemo2/Hw_Out")
            {
                if (mensaje == "Movimiento")
                {
                    movimientos.Add(new Movimiento());
                    nuevoMovimiento = true;
                }
                if (mensaje.StartsWith("Variables"))
                {
                    string[] datos = mensaje.Split('|');
                    valPotenciometro = float.Parse(datos[1]);
                    valHumedad = float.Parse(datos[2]);
                    valLuminosidad = float.Parse(datos[3]);
                    BuzerActivo = datos[4] == "True";
                    lucesEncendidas = datos[5] == "True";
                    valContador = int.Parse(datos[6]);
                    if (lecturas.Count > 30)
                    {
                        lecturas.RemoveRange(0, 10);
                    }
                    lecturas.Add(new Microcharts.ChartEntry(valLuminosidad)
                    {
                        Label = DateTime.Now.ToLongTimeString(),
                        ValueLabel=Math.Round(decimal.Parse(datos[3]),2).ToString(),
                        Color=new SkiaSharp.SKColor(255,255,255),
                        ValueLabelColor=new SkiaSharp.SKColor(255, 255, 255)
                    });
                }
            }
        }

        private void btnConectar_Clicked(object sender, EventArgs e)
        {
            clientMqtt = new MQTTClient("broker.hivemq.com", 1883);
            clientMqtt.MessageReceived += ClientMqtt_MessageReceived;
            clientMqtt.Connect(Guid.NewGuid().ToString().Substring(0, 10));
            while (!clientMqtt.IsConnected)
            {
                Thread.Sleep(1000);
            }
            clientMqtt.Subscriptions.Add(new Subscription("NFDemo2/Hw_Out"));
            DisplayAlert("SimpleMqttClient", "Conectado", "Ok");
            btnConectar.IsEnabled = false;
            gridControlres.IsEnabled = true;
            Device.StartTimer(TimeSpan.FromMilliseconds(200), () =>
            {
                if (nuevoMensaje)
                {
                    nuevoMensaje = false;
                    lstMensajes.ItemsSource = null;
                    lstMensajes.ItemsSource = mensajes.OrderByDescending(m => m.FechaHora);

                    gaugeHumedadValue.Value = valHumedad;
                    gaugeLuminosidadValue.Value = valLuminosidad;
                    gaugePotenciometroValue.Value = valPotenciometro;
                    lblContadorValue.Text = valContador.ToString();
                    if (BuzerActivo)
                    {
                        lblStatusBuzzer.Text = "Alarma encendida";
                    }
                    else
                    {
                        lblStatusBuzzer.Text = "Alarma apagada";
                    }
                    if (lucesEncendidas)
                    {
                        lblStatusLuces.Text = "Luces encendidas";
                    }
                    else
                    {
                        lblStatusLuces.Text = "Luces apagadas";
                    }
                    var grafico = new LineChart()
                    {
                        Entries = lecturas,
                        LabelTextSize=10,
                        BackgroundColor= new SkiaSharp.SKColor(0, 0, 0),
                    };
                    Grafico.Chart = grafico;
                }
                if (nuevoMovimiento)
                {
                    nuevoMovimiento = false;
                    lstMovimiento.ItemsSource = null;
                    lstMovimiento.ItemsSource = movimientos.OrderByDescending(m => m.FechaHora);
                }
                return true;
            });
        }

        private void btnEncenderLuces_Clicked(object sender, EventArgs e)
        {
            EnviarComando("L1");
        }

        private void EnviarComando(string comando)
        {
            clientMqtt.Publish("NFDemo2/Hw_In", comando, QoS.FireAndForget, false);
        }

        private void btnApagarLuces_Clicked(object sender, EventArgs e)
        {
            EnviarComando("L0");
        }

        private void btnEncenderBuzzer_Clicked(object sender, EventArgs e)
        {
            EnviarComando("B1");
        }

        private void btnApagarBuzzer_Clicked(object sender, EventArgs e)
        {
            EnviarComando("B0");
        }

        private void btnEnviarPasos_Clicked(object sender, EventArgs e)
        {
            int pasos = 0;
            if(int.TryParse(entPasos.Text,out pasos))
            {
                if (pasos > 0)
                {
                    EnviarComando($"M{pasos}");
                }
                else
                {
                    DisplayAlert("Error", "Los pasos deben ser mayor a cero", "Ok");
                }
            }
            else
            {
                DisplayAlert("Error", "Los pasos deben ser un valor numérico y mayor a cero", "Ok");
            }
        }
    }
}
