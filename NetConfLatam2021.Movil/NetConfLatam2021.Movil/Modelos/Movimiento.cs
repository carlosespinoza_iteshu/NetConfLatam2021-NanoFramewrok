﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetConfLatam2021.Movil.Modelos
{
    public class Movimiento
    {
        public DateTime FechaHora { get; set; }
        public string Contenido { get; set; }

        public Movimiento()
        {
            FechaHora = DateTime.Now;
            Contenido = DateTime.Now.ToLongTimeString();
        }

        public override string ToString()
        {
            return Contenido;
        }
    }
}
