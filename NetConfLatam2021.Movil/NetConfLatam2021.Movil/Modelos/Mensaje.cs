﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetConfLatam2021.Movil.Modelos
{
    public class Mensaje
    {
        public DateTime FechaHora { get; set; }
        public string Topico { get; set; }
        public string Contenido { get; set; }

        public Mensaje(string topico, string contenido)
        {
            FechaHora = DateTime.Now;
            Topico = topico;
            Contenido = contenido;
        }
        public override string ToString()
        {
            return $"{FechaHora.ToLongTimeString()}=>[{Topico}]{Contenido}";
        }
    }
}
